//get the total of the prices
export const getTotal = (arr) =>{
    const initialValue = 0;
    const totalPrice = arr.reduce(
        (pV, cV) => pV + (cV.price * cV.qty),
        initialValue
      );
    return totalPrice;
}