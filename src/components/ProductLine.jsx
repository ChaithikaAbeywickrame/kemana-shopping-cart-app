import React, {useState} from 'react';
import { useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useStore } from '../store/Store';

//created to render each product item on the list
const ProductLine = ({item}) => {
    const {
        productBasket,
        addNewItem,
        reduceQty,
      } = useStore();

    //get the product details on load and populate
    const proArr = productBasket.filter((ele)=>ele.id === item.id)
    const [selectedQty, setSelectedQty] = useState((proArr.length === 1)? proArr[0].qty : 0)
    const [total, setTotal] = useState(0)

    //calc total of each item based on the quantity
    const calcTotal = () => {
        setTotal(item.price * selectedQty)
    }

    //called everytime the selected qty is changed
    useEffect(()=>{
        calcTotal()
    },[selectedQty])

    //to clear the values when the cart is empty
    useEffect(()=>{
        if(productBasket.length === 0){
            setSelectedQty(0)
        }
        let proItem = productBasket.filter((ele)=>ele.id === item.id)
        if(proItem.length === 1){
            setSelectedQty(proItem[0].qty)
        }else{
            setSelectedQty(0)
        }
    },[productBasket])

    return (
        <tr>
            <td>{item.id}</td>
            <td>{item.title}</td>
            <td>{item.stock}</td>
            <td>LKR. {item.price}</td>
            <td>
                <Button onClick={()=>reduceQty(item)}>-</Button>
                <span className='px-3'>{selectedQty}</span>
                <Button onClick={()=>addNewItem(item)}>+</Button>
            </td>
            <td>
                Total: LKR. {total}
            </td>
        </tr>
    );
};

export default ProductLine;