import React from 'react';
import {Modal, Button, Table} from "react-bootstrap"
import {useStore} from '../store/Store'
import { getTotal } from '../utilities/utilities';
import ShoppingCartLine from './ShoppingCartLine';

const ShoppingCartModal = ({handleClose, show}) => {
    const {
        productBasket,
        clearCart
    } = useStore();

    //function to empty the cart
    const handleEmptyCart = () => {
        if(productBasket.length === 0) {
            alert("Your cart is already empty"); 
            return
        }
        if(window.confirm("Are you sure you want to clear the cart?")){
            clearCart()
        }
    }

    return (
        <Modal show={show} onHide={handleClose} size="lg">
            <Modal.Header closeButton>
                <Modal.Title>View Your Cart!</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {(productBasket.length === 0)?
                    "Your cart is empty! Please add items to Cart":
                    <Table>
                        <thead>
                            <tr>
                                <td>Title</td>
                                <td>Quantity</td>
                                <td>Unit Price</td>
                                <td>Price</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            {/* draw each item on the cart */}
                            {productBasket.map((product)=>{
                                return(
                                    <ShoppingCartLine key={product.id} product={product}/>
                                )
                            })}
                            <tr>
                                <td></td>
                                <td></td>
                                <td><strong>Total</strong></td>
                                <td>LKR. {getTotal(productBasket)}</td>
                            </tr>
                        </tbody>
                    </Table>
                }
                
            </Modal.Body>
            <Modal.Footer>
                {/* empty cart button is visible only when there are cart items */}
                {productBasket.length !== 0 ? 
                    <Button variant="warning" onClick={()=>{handleEmptyCart()}}>
                        Empty Cart
                    </Button>
                    :<></>
                }
                
                <Button variant="primary" onClick={handleClose}>
                    Continue Shopping
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default ShoppingCartModal;