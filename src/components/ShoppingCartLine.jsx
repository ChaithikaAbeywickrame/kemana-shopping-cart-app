import React from 'react';
import {useStore} from '../store/Store'
import {Button} from 'react-bootstrap';

const ShoppingCartLine = ({product}) => {
    const {
        addNewItem,
        reduceQty,
      } = useStore();
    return (
        <tr>
            <td>{product.title}</td>
            <td>
                <Button variant="outline-dark" onClick={()=>reduceQty(product)}>-</Button>
                    <span className='px-3'>{product.qty}</span>
                <Button variant="outline-dark" onClick={()=>addNewItem(product)}>+</Button>
            </td>
            <td>LKR. {product.price}</td>
            <td>LKR. {product.qty * product.price}</td>
        </tr>
    );
};

export default ShoppingCartLine;