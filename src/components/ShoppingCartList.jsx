import React from 'react';
import { useStore } from '../store/Store';
import { Table, Button } from 'react-bootstrap';
import ShoppingCartLine from './ShoppingCartLine';
import { useNavigate } from 'react-router-dom';
import { getTotal } from '../utilities/utilities';

const ShoppingCartList = () => {
    const {
        productBasket,
        clearCart,
      } = useStore()
    
    const navigate = useNavigate();
    //get the total tax based on the cart items
    const totalTax = getTotal(productBasket) * 0.0123;
    //get the total price along with the shipping price
    const totalPrice = getTotal(productBasket) + totalTax + 500;
    const handleClick = () =>{
        navigate('/products/');
    }
    const handlePayment = () =>{
        clearCart()
        navigate('/thank-you');
    }
    
    return (
        <>
            <Table>
                <thead>
                    <tr>
                        <td>Title</td>
                        <td>Quantity</td>
                        <td>Unit Price</td>
                        <td>Total Price</td>
                    </tr>
                </thead>
                <tbody>
                    {/* draw each item on the cart */}
                    {productBasket.map((product)=>{
                        return(
                            <ShoppingCartLine key={product.id} product={product}/>
                        )
                    })}
                    <tr>
                        <td></td>
                        <td></td>
                        <td><strong>Total Cost of Products</strong></td>
                        <td>LKR. {getTotal(productBasket)}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><strong>Total Tax on Items (1.23%)</strong></td>
                        <td>LKR. {totalTax.toFixed(2)}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><strong>Shipping Cost</strong></td>
                        <td>LKR. 500</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><strong>Total Checkout Price</strong></td>
                        <td>LKR. {totalPrice.toFixed(2)}</td>
                    </tr>
                </tbody>
            </Table>
            <div className='d-flex justify-content-end gap-3'>
                <Button variant="success" onClick={()=>handleClick()}>Go Back to Shopping</Button>
                <Button variant="primary" onClick={()=>handlePayment()}>Pay Now</Button>
            </div>
        </>
    );
};

export default ShoppingCartList;