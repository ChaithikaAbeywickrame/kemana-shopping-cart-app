import React, {useState, useEffect} from 'react';
import ProductData from '../data/products.json'
import ProductLine from './ProductLine';
import {Table, Button} from 'react-bootstrap';
import { useStore } from '../store/Store';
import { useNavigate } from 'react-router-dom';
import ShoppingCartModal from './ShoppingCartModal';

const ProductList = () => {
    const {
        productBasket,
      } = useStore();
    //data is taken from the json file added to the solution
    const data = ProductData;
    const navigate = useNavigate();

    //methods and state for the modal
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    //navigation to the checkout page
    const handleClick = () =>{
        if(productBasket.length === 0){
            alert("Your cart is emply!!")
            return
        }
        navigate('/products/cart');
    }

    //update the local storage everytime the product basket is updated
    useEffect(() =>{
        localStorage.setItem("CartItems",JSON.stringify(productBasket))
      },[productBasket])

    return (
        <div>
            <div className='d-flex justify-content-end gap-3'>
                <Button variant="success" onClick={()=>handleClick()}>Go To Checkout</Button>{' '}
                <Button variant="primary" onClick={handleShow}>View Cart</Button>
            </div>
            <Table striped>
                <thead>
                    <tr>
                        <td><strong>ID</strong></td>
                        <td><strong>Title</strong></td>
                        <td><strong>Quantity</strong></td>
                        <td><strong>Unit Price</strong></td>
                    </tr>
                </thead>
                <tbody>
                    {/* Looped through each array item to draw the products */}
                    {data && data.map((item)=>(
                        <ProductLine key={item.id} item={item}/>
                    ))}
                </tbody>
            </Table>
            <ShoppingCartModal handleClose={handleClose} show={show} 
            />
        </div>
    );
};

export default ProductList;