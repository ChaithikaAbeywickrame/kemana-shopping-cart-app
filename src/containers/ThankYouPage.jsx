import React from 'react';
import { Container } from 'react-bootstrap';

const ThankYouPage = () => {
    return (
        <div>
            <Container>
                <h1>Thank you! Your Order has been placed Successfully</h1>
            </Container>
        </div>
    );
};

export default ThankYouPage;