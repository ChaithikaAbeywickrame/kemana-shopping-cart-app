import React from 'react';
import { Container } from 'react-bootstrap';
import ProductList from '../components/ProductList';

const ProductsPage = () => {
    return (
        <div>
            <Container>
                <h1>Product List</h1>
                <ProductList/>
            </Container>
        </div>
    );
};

export default ProductsPage;