import React from 'react';
import {Container} from 'react-bootstrap'
import ShoppingCartList from '../components/ShoppingCartList';

const ShoppingCart = () => {
    return (
        <div>
            <Container>
                <h1>Shopping Cart</h1>
                <ShoppingCartList/>
            </Container>
        </div>
    );
};

export default ShoppingCart;<h1>Shopping Cart</h1>