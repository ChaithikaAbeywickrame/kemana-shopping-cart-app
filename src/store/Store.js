
import create from 'zustand'

export const useStore = create((set) => ({
  productBasket: JSON.parse(localStorage.getItem("CartItems")!=null )? JSON.parse(localStorage.getItem("CartItems")) : [],
  
  addNewItem: (newProduct) =>{
    set((state)=>({
        productBasket : 
          (!state.productBasket.find(elem=> elem.id=== newProduct.id)
            ?[...state.productBasket, {...newProduct, qty: newProduct.qty+1}]
            : state.productBasket.map((product)=>(
              product.id === newProduct.id ? {...product, qty: product.qty+1} : product
            ))
          )
    }))
  },
  reduceQty: (product) =>{
    set((state)=>({
        productBasket :
        (state.productBasket.find(elem => (elem.id === product.id) && (elem.qty !== 1))
          ?state.productBasket.map((pd)=>(
            pd.id === product.id ? {...pd, qty: pd.qty - 1} : pd
          ))
          : state.productBasket.filter((item)=> item.id !== product.id)
        )
        
    }))
  },
  clearCart: ()=>{
    set((state)=>({
        productBasket : [],
    }))
    localStorage.clear()
  }
}))