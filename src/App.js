import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import ProductsPage from './containers/ProductsPage';
import ShoppingCart from './containers/ShoppingCart';
import ThankYouPage from './containers/ThankYouPage';

function App() {
  return (
    <BrowserRouter>
      <Routes>
          <Route path="/products" element={<ProductsPage />}/>
          <Route index element={<ProductsPage />} />
          <Route path="/products/cart" element={<ShoppingCart />} />
          <Route path="/thank-you" element={<ThankYouPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
